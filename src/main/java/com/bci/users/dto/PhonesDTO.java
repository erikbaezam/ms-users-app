package com.bci.users.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class PhonesDTO {


    @JsonProperty(value = "number")
    private String number;
    @JsonProperty(value = "citycode")
    private String cityCode;
    @JsonProperty(value = "contrycode")
    private String countryCode;



}
