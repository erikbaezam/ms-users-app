package com.bci.users.validator;

import com.bci.users.dto.UserRequestDTO;
import com.bci.users.exception.BadRequestException;
import com.bci.users.util.Utils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.Objects;
import java.util.Optional;

@Component
public class ValidationHandler {

    public void validateUserRequestDTO(UserRequestDTO body) {

        if (body == null) {
            throw new BadRequestException("COD-002 | Request null");
        }

        Optional.ofNullable(body).filter(x -> StringUtils.isNotBlank(body.getEmail()))
                .orElseThrow(() -> new BadRequestException("COD-003 | Email Address  cannot be empty"));

        Optional.ofNullable(body).filter(x -> Utils.emailValidation.apply(body.getEmail()))
                .orElseThrow(() -> new BadRequestException("COD-004 | Email Address is invalid"));

        Optional.ofNullable(body).filter(x -> StringUtils.isNotBlank(body.getName()))
                .orElseThrow(() -> new BadRequestException("COD-005 | Name cannot be empty"));

        Optional.ofNullable(body).filter(x -> StringUtils.isNotBlank(body.getPassword()))
                .orElseThrow(() -> new BadRequestException("COD-006 | Password cannot be empty"));

        Optional.ofNullable(body).filter(x -> Utils.passwordValidation.apply(body.getPassword()))
                .orElseThrow(() -> new BadRequestException("COD-007 | Invalid Password Format (Contains at least 8 characters and a maximum of 20 characters.\n" +
                        "        Contains at least one digit.\n" +
                        "        Contains at least one uppercase alphabet.\n" +
                        "        Contains at least one lowercase alphabet.\n" +
                        "        Contains at least one special character including ! @ # $% & *() - + = ^ .\n" +
                        "        Does not contain any blank spaces."));

        Optional.ofNullable(body).filter(x -> Objects.nonNull(x.getPhones()))
                .orElseThrow(() -> new BadRequestException("COD-008 | Phones cannot be empty"));

        Optional.ofNullable(body.getPhones()).map(list -> list.stream()
                .filter(Objects::nonNull)
                .filter(e ->
                        StringUtils.isNotBlank(e.getNumber())
                                && StringUtils.isNotBlank(e.getCityCode())
                                && StringUtils.isNotBlank(e.getCountryCode()))).orElseThrow(() ->
                new BadRequestException("COD-009 | Phones Data cannot be empty"));

    }
}
