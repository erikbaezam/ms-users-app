package com.bci.users.service;

import com.bci.users.dto.UserDTO;
import com.bci.users.dto.UserRequestDTO;


public interface UsersService {

    public UserDTO saveUser(UserRequestDTO userRequestDTO);

}
