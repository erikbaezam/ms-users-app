package com.bci.users.service;

import com.bci.users.dto.PhonesDTO;
import com.bci.users.dto.UserDTO;
import com.bci.users.dto.UserRequestDTO;
import com.bci.users.exception.ErrorException;
import com.bci.users.model.Phones;
import com.bci.users.model.User;
import com.bci.users.repository.UsersRepository;
import com.bci.users.util.Utils;
import com.bci.users.validator.ValidationHandler;
import lombok.extern.slf4j.Slf4j;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;


@Slf4j
@Service
public class UsersServiceImpl implements UsersService {

    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    private ValidationHandler validationHandler;


    static DozerBeanMapper mapper = new DozerBeanMapper();


    @Override
    public UserDTO saveUser(UserRequestDTO userRequestDTO) {
        validationHandler.validateUserRequestDTO(userRequestDTO);
        UserDTO response = UserDTO.builder().build();
        UserDTO user = this.validateEmail.apply(userRequestDTO.getEmail());
        if (user != null && null != user.getId()) {
            throw new ErrorException("COD-001 | Email already register");
        }
        try {
            response = this.saveUser.apply(UserDTO.builder()
                    .created(new Date())
                    .email(userRequestDTO.getEmail())
                    .isActive(Boolean.TRUE)
                    .modified(new Date())
                    .lastLogin(new Date())
                    .name(userRequestDTO.getName())
                    .password(userRequestDTO.getPassword())
                    .token(this.createId.get())
                    .build(), userRequestDTO.getPhones());

        } catch (ErrorException exception) {
            throw new ErrorException("COD-010 | An error occurred while saving the user");
        }
        return response;
    }

    private final Supplier<String> createId = () -> UUID.randomUUID().toString().replace("-", "").substring(0, 9);

    private final BiFunction<UserDTO, List<PhonesDTO>, UserDTO> saveUser = (UserDTO userDTO, List<PhonesDTO> phoneList) -> {

        try {
            User user = mapper.map(userDTO, User.class);
            List<Phones> listPhones = new ArrayList<Phones>();
            for (PhonesDTO phonesDTO : phoneList) {
                listPhones.add(mapper.map(phonesDTO, Phones.class));
            }
            listPhones.stream().forEach(p -> p.setUser(user));
            user.setPhones(listPhones);
            User response = usersRepository.save(user);
            log.info("Call to repository -> saveUser Response: {}", user);
            return Optional.ofNullable(user)
                    .filter(validate -> user.getId() != null)
                    .map(value -> Utils.userConverter.apply(user))
                    .orElse(null);
        } catch (Exception e) {
            throw new ErrorException("COD-010 | An error occurred while saving the user");
        }
    };
    private final Function<String, UserDTO> validateEmail = (String email) -> {
        try {
            User user = usersRepository.findByEmail(email);
            log.info("Call to repository -> validateEmail Response: {}", user);
            return Optional.ofNullable(user)
                    .filter(validate -> user.getId() != null)
                    .map(value -> Utils.userConverter.apply(user))
                    .orElse(null);
        } catch (Exception e) {
            throw new ErrorException("COD-011 | An error occurred while getting the user");
        }
    };

}
