package com.bci.users.util;

public class ConstantsUtil {

    private ConstantsUtil() {
    }

    public static final String EMAIL_REGX = "^(.+)@(.+)$";

    /*  Contains at least 8 characters and a maximum of 20 characters.
        Contains at least one digit.
        Contains at least one uppercase alphabet.
        Contains at least one lowercase alphabet.
        Contains at least one special character including ! @ # $% & *() - + = ^ .
        Does not contain any blank spaces.
    */
    public static final String PASSWORD_REGX = "^(?=.*[0-9])"
            + "(?=.*[a-z])(?=.*[A-Z])"
            + "(?=.*[@#$%^&+=])"
            + "(?=\\S+$).{8,20}$";
    ;
}
