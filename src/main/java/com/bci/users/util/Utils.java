package com.bci.users.util;

import com.bci.users.dto.UserDTO;
import com.bci.users.model.User;

import org.dozer.DozerBeanMapper;

import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {


    static DozerBeanMapper mapper = new DozerBeanMapper();

    private Utils() {
    }

    public static Function<User, UserDTO> userConverter = (User user) -> mapper.map(user, UserDTO.class);

    public static Function<String, Boolean> emailValidation =
            (String email) -> {
                Pattern pattern = Pattern.compile(ConstantsUtil.EMAIL_REGX);
                Matcher matcher = pattern.matcher(email);
                return matcher.matches();
            };
    public static Function<String, Boolean> passwordValidation =
            (String pass) -> {
                Pattern pattern = Pattern.compile(ConstantsUtil.PASSWORD_REGX);
                Matcher matcher = pattern.matcher(pass);
                return matcher.matches();
            };
}
