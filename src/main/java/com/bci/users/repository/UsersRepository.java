package com.bci.users.repository;

import com.bci.users.model.User;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UsersRepository extends JpaRepository<User, Integer> {

    public User findByEmail(String email);
}
