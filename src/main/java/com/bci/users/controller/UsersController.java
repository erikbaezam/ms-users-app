package com.bci.users.controller;

import com.bci.users.dto.UserDTO;
import com.bci.users.dto.UserRequestDTO;
import com.bci.users.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UsersController {

    private final UsersService usersService;

    @Autowired
    UsersController(UsersService usersService){
        this.usersService = usersService;
    }

    @PostMapping(value = "/users", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<UserDTO> saveUser(
            @RequestBody UserRequestDTO request) {
        return ResponseEntity.ok(usersService.saveUser(request));
    }

}
