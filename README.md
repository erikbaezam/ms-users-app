MS-USERS
=========================

Microservice in charge of managing system users.


---
## MS-USERS Usage

- To get started using MS-USERS, please go to https://gitlab.com/erikbaezam/ms-users-app and clone the repository https://gitlab.com/erikbaezam/ms-users-app.git
- You have two options to execute te API 
  - Option one: Inside the repo you have the JAR executable. To run the app you need to open a CMD and use the following command java -jar "PERSONAL-RUTE\ms-users-app\build\libs\users-0.0.1-SNAPSHOT"
  - Option two: Open the repo in your favourite IDE. 
- When the API is Start, open yor testing application and add the POST endpoint  " /users " and copy the following JSON object:

{
    "name": "",
    "email": "",
    "password": "",
    "phones": [
        {
            "number": "",
            "citycode": "",
            "contrycode": ""
        }
    ]
}

-If you add the corresponding data, it is persisted in the database.


## Main Features
- Scalable application
- Easy to deploy

---
